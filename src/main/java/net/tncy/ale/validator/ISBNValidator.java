package net.tncy.ale.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
        // Algorithme de validation du numéro ISBN - Début
        valid = true;
        // Algorithme de validation du numéro ISBN - Fin
        return valid;
    }
}